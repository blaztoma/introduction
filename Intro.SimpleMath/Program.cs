﻿using System;

namespace Intro.SimpleMath
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite z reikšmę:");
            int z = int.Parse(Console.ReadLine());

            double result = Math.Sqrt(z - 1);

            if (double.IsNaN(result))
            {
                Console.WriteLine(" z = {0}  f-ja neegzistuoja", z);
            }
            else
            {
                Console.WriteLine(" z = {0} f(x) =  {1}", z, result);
            }

            Console.ReadKey();
        }
    }
}
