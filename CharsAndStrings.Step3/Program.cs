﻿using System;

namespace Intro.CharsAndStrings.Step3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite raidę raidžių intervalo pradžiai");
            char startChar = Console.ReadLine()[0];

            Console.WriteLine("Įveskite raidę raidžių intervalo pabaigai");
            char finishChar = (char)Console.Read();

            for (char ch = startChar; ch <= finishChar; ch++)
            {
                Console.WriteLine("{0} - {1}", ch, (int)ch);
            }

            Console.ReadKey();
        }
    }
}
