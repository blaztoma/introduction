﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro.trials
{
    class Program
    {
        static void Main(string[] args)
        {
            int k = CalculateDiff(CalculateDiff(13, 8), CalculateDiff(57 / 4, 20));

            Console.WriteLine("{0}",k);
            Console.ReadKey();
        }

        static int AddNumbers(int a, int b)
        {
            return a + b;
        }

        static int FindGreater(int a, int b)
        {
            if (a > b) return a;
            return b;
        }

        static int CalculateDiff(int a, int b)
        {
            if (a > b) return a - b;
            return b - a;
        }

    }
}
