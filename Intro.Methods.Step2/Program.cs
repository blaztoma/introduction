﻿using System;

namespace Intro.Methods.Step2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite a reikšmę:");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("Įveskite x reikšmę:");
            double x = double.Parse(Console.ReadLine());

            double result = CalculateFunctionValue(a, x);
            Console.WriteLine(" Reikšmė a = {0}, reikšmė x = {1}, fx = {2}", a, x, result);

            Console.ReadKey();
        }

        static double CalculateFunctionValue(int a, double x)
        {
            double result;

            if (x <= 0)
            {
                result = a * Math.Exp(-x);
            }
            else if (x < 1)
            {
                result = 5 * a * x - 7;
            }
            else
            {
                result = Math.Sqrt(x + 1);
            }

            return result;
        }
    }
}
