﻿using System;

namespace Intro.Methods.Step1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite spausdinamą simbolį");
            char character = (char)Console.Read();

            Print(character);
            Console.ReadKey();
        }

        static void Print(char characterToPrint)
        {
            for (int i = 1; i < 51; i++)
            {
                if (i % 5 == 0)
                {
                    Console.WriteLine(characterToPrint);
                }
                else
                {
                    Console.Write(characterToPrint);
                }
            }

            Console.WriteLine();
        }
    }
}
