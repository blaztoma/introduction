﻿using System;

namespace Intro.If.Step2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite spausdinamą simbolį");
            char character = (char)Console.Read();

            for (int i = 1; i < 51; i++)
            {
                if (i % 5 == 0)
                {
                    Console.WriteLine(character);
                }
                else
                {
                    Console.Write(character);
                }
            }

            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
