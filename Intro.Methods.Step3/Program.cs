﻿using System;

namespace Intro.Methods.Step3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Įveskite z reikšmę:");
            int z = int.Parse(Console.ReadLine());

            double result = CalculateFunctionValue(z, 1, 0.5);

            if(double.IsNaN(result))
            {
                Console.WriteLine(" z = {0}  f-ja neegzistuoja", z);
            }
            else
            {
                Console.WriteLine(" z = {0} f(x) =  {1}", z, result);
            }

            Console.ReadKey();
        }

        static double CalculateFunctionValue(int value1, int value2, double power)
        {
            return Math.Pow(value1 - value2, power);
        }
    }
}
